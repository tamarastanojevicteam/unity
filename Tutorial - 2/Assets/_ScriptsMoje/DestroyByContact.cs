﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByContact : MonoBehaviour {

    
    public GameObject playerExplosion;
    public GameObject explosion;
    public int scoreValue; //toliko se dodaje kad ga pogodi

    private GameController gameController; //u pitanju je hazard koji se pojavljuje tek kada pocne igra
    //i svaki ima svoju instancu na GameCOntroller, pa ne radi samo addScore,vec se svakoj instanci 
    //hazarda mora dodati instanca GameControllera u toku igre kad se pojavi
    //tako sto preko taga trazi taj gameController
    //tj najpre game object koji ima tag game controller
    //ako uspesno nadjemo taj game object, tj nije null, setujemo gameCOntroller na componentu tj scriptu
    //tog game objecta

    void Start()
    {
        GameObject gameControllerObject = GameObject.FindWithTag("GameController");

        if(gameControllerObject!=null)
        {
            gameController = gameControllerObject.GetComponent<GameController>();
        }
        if(gameController==null)
        {
            Debug.Log("Cannot find game controller script");
        }
    }
    void OnTriggerEnter(Collider other)
    {
        
        if (other.tag == "Boundary")
        {
            return;
        }
        Instantiate(explosion, transform.position, transform.rotation);

        if (other.tag == "Player")
        {
            Instantiate(playerExplosion, other.transform.position, other.transform.rotation);
            gameController.GameOver();
        }

        gameController.AddScore(scoreValue);
        Destroy(other.gameObject);
        Destroy(gameObject);

    }

}
