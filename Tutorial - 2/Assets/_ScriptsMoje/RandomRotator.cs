﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomRotator : MonoBehaviour {
    public float tumble;

    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        //how fast rigidbody is rotating - angularVelocity
        //ali vremenom usporava...
        //pa stavimo Angular Drag na 0
        rb.angularVelocity = Random.insideUnitSphere * tumble;
    }
}
