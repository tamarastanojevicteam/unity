﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyByBoundary : MonoBehaviour {

    //javlja se kada se zavrsi triger, tj kada triger napusti collider
	void OnTriggerExit(Collider other)
    {
        Destroy(other.gameObject);
    }
}
