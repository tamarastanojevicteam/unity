﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public GameObject player;
    private Vector3 offset;

	// Use this for initialization
	void Start () {
        //offset je razlika izmedju pozicije kamere i pozicije player-a
        offset = transform.position - player.transform.position;
	}
	
	// Update is called once per frame
    //LateUpdate je isto sto i Update ali se zagarantovano desi nakon sto su svi objekti 
    //update-ovani nakon Update() funkcije 
    //pa znamo sigurno da se player pomerio za taj frame i zato mozemo kameru da pomerimo
	void LateUpdate () {
        //pomeranje kamere sa svakim klikom za promenu polozaja player-a
        transform.position = player.transform.position + offset;
	}
}
