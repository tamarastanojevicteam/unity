﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    public float speed;
    public Text countText;
    public Text winText;
    public Text timeText;
 
    private Rigidbody rb;
    private int count;
    private float time;
    private bool kraj = false;

   
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winText.text = "";
        time = 20.0f;
    }
    
    void Update()
    {
        if (count < 8)
        {
            time -= Time.deltaTime;
            if (time < 0)
            {
                GameOver();
            }
            else
            {
                SetTimeText();
            }
        }
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");
                                                    //y je za gore dole - 0 za nas
        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement*speed);
    }

    //other je objekat koji dotaknemo i njega cemo da unistimo 
    void OnTriggerEnter(Collider other)
    {
        if (!kraj)
        {
            if (other.gameObject.CompareTag("Pick Up"))
            {
                other.gameObject.SetActive(false);
                count++;
                SetCountText();
            }
        }
        else
        {
            other.isTrigger = false;
        }
    }

    void SetCountText()
    {
        countText.text = "Score: " + count.ToString();
        if (count >= 8)
            winText.text = "Pobedili ste!";
    }

    void SetTimeText()
    {
        int t = (int)time;
        timeText.text = "Time: " + t.ToString()+" s";
    }

    void GameOver()
    {
        if (count < 8)
        {
            winText.text = "Zao mi je, izgubili ste!";
            kraj = true;
        }
        else
            winText.text = "Bravo, pobedili ste!";


    }
}
