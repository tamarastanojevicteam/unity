﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {

    public Transform target;
    public float smoothing = 5f; //za pracenje player-a

    private Vector3 offset; //od kamere do player-a

    void Start()
    {
        offset = transform.position - target.position; //target je player
    }

    void FixedUpdate()
    {
        Vector3 targetCamPos = target.position + offset;
        transform.position = Vector3.Lerp(transform.position, targetCamPos, smoothing*Time.deltaTime); //50 puta u sekundi pa ide * Time.deltatime
            //lerp smoothly moves between two positions
    }






}
