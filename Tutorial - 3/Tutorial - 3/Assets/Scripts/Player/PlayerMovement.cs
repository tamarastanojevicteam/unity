﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float speed = 6f;

    private Vector3 movement;
    private Animator anime;
    private Rigidbody rb;
    private int floorMask;
    private float camRayLength = 100f;


    void Awake()
    {
        floorMask = LayerMask.GetMask("Floor");
        anime = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        //-1 ili 0 ili 1 = GetAxisRaw, pa se momentalno pomeri a ne sa ubrzanjem polako
        float h = Input.GetAxisRaw("Horizontal");
        float v = Input.GetAxisRaw("Vertical");

        Move(h, v);
        Turning();
        Animating(h, v);
    }

    //kada kazemo levo ide 1 levo, gore ide 1 gore, ali ako drzimo oba istovremeno ide 1.4 dijagonalno, da ne bi isao dijagonalno, 
    //vrsimo normalizaciju pokreta 
    //deltaTime je vreme izmedju dva update poziva
    void Move(float h, float v)
    {
        movement.Set(h, 0f, v);

        // Normalise the movement vector and make it proportional to the speed per second.
        movement = movement.normalized * speed * Time.deltaTime;

        // Move the player to it's current position plus the movement.
        rb.MovePosition(transform.position + movement);
         //dodajemo pokret igracu na njegov trenutni polozaj
    }

    void Turning()
    {
        //zrak kamere se reflektuje na scenu
        Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        //gde je udario zrak
        RaycastHit floorHit;

        if (Physics.Raycast(camRay, out floorHit, camRayLength, floorMask))
        {
            Vector3 playerToMouse = floorHit.point - transform.position;
            playerToMouse.y = 0f;

            Quaternion newRotation = Quaternion.LookRotation(playerToMouse);
            rb.MoveRotation(newRotation);
        }

    }

    void Animating(float h, float v)
    {
        //ako su v ili h razliciti od nule znaci da se krece i da je true, u suprotnom je 0 i false
        bool walking = h != 0f || v != 0f;

        anime.SetBool("IsWalking", walking);
    }



}
